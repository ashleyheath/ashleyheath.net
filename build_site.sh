#!/bin/bash

set -o errexit

root="site/"
#"/home/ashley/ashleyheath.net/site/"

template=$root"template_html/template.html"

content_html=$root"content_html/"
static_html=$root"static_html/"

html_pages="build_scripts/html_pages.txt"

# Build a list of the html pages in content_html
find $content_html -type f -name '*.html' > $html_pages

# Spell check pages using aspell
while read line
do
  files+=( "$line" )
done < $html_pages

for file in "${files[@]}"
do
  echo "Spell checking $file... "
  aspell check --mode='html' --add-filter='url' --lang 'en_UK' "$file"
  echo "Checked."
done

# Render pages
python "build_scripts/page_renderer.py" $template $content_html $static_html $html_pages

# Change $pages to point to static_html rather than content_html
sed -i s/content/static/ $html_pages # replace 'content' with 'static' in filepaths # -i : edit files in place
### !! So can't have 'content' or 'static' in title !!###

# Fix relative paths, title, last update date etc.
python "build_scripts/page_fixer.py" $root $html_pages

# Update the 'Last Updated' section of humans.txt and template.html
new_date=$(date +%F)
sed -i "/Last Updated:/ c \Last Updated: $new_date" $root"humans.txt"

# Cleanup
rm $html_pages
