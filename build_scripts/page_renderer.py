#!/usr/bin/env python
"""Replace {!content!} tags in a .html template with custom content and change the 'last updated' date."""

import sys
import os.path
import datetime

usage = "Usage: page_renderer /path/to/template.html /path/to/contents.html"

content_tag = '{!content!}'
last_updated_tag = '{!last_updated!}'

last_updated_string = datetime.datetime.now().date().strftime('%d %b %Y')

def fail_and_exit(message):
  print message
  exit()

def page_renderer(template_path, content_html_path, static_html_path, page_paths):
  template_file = open(template_path, 'r')
  template = template_file.read()

  # Update the last update date
  # We do this here rather than in page_fixer to avoid making the change for each page
  template = template.replace(last_updated_tag, last_updated_string)

  contents_start = template.find(content_tag)
  template_file.close()
  
  if contents_start == -1:
    fail_and_exit("No </content/> tag found, exiting...")
  template_front = template[0:contents_start]
  template_back = template[contents_start+len(content_tag):]
  
  if not os.path.exists(static_html_path):
    os.makedirs(static_html_path)
    
  for i in range(len(page_paths)):
    print "Rendering ", page_paths[i], '... ',
    content_file = open(page_paths[i], 'r')
    content = content_file.read()
    content_file.close()
    content_name = (page_paths[i])[page_paths[i].rfind('/'):]
    
    content_directory = (page_paths[i])[:page_paths[i].rfind('/')+1]
    content_directory = content_directory.replace(content_html_path, '')
    content_directory = static_html_path + content_directory
    if not os.path.exists(content_directory):
      os.makedirs(content_directory)
      
    rendered_page_path = content_directory + content_name
    rendered_page = open(rendered_page_path, 'w')
    rendered_page.write(template_front + content + template_back)
    rendered_page.close()
    print "Rendered."

def main(argv):
  TEMPLATE_FILEPATH = argv[0]
  CONTENT_HTML_FILEPATH = argv[1]
  STATIC_HTML_FILEPATH = argv[2]
  PAGES_LIST_FILEPATH = argv[3]
  
  pages_list_file = open(PAGES_LIST_FILEPATH, 'r')
  pages_list = pages_list_file.readlines()
  pages_list_file.close()
  
  for i in range(len(pages_list)):
    pages_list[i] = pages_list[i].replace('\n', '')
  
  page_renderer(TEMPLATE_FILEPATH, CONTENT_HTML_FILEPATH, STATIC_HTML_FILEPATH, pages_list)
  
if __name__ == '__main__':
  main(sys.argv[1:])
