#!/usr/bin/env python
"""Fix rendered pages based on the tags they contain."""

import sys

static_path_tag = '{!static_path!}'
title_tag = '{!title!}'

def fix_pages(root_path, page_paths):
  for path in page_paths:
    print "Fixing ", path, '... ',
    html_file = open(path, 'r')
    html = html_file.read()
    html_file.close()
    # Fix static paths
    path_from_root = path.replace(root_path, '')
    level = path_from_root.count('/')
    html = html.replace(static_path_tag, '../'*level)
    # Fix title
    title = path[path.rfind('/')+1:path.rfind('.')]
    html = html.replace(title_tag, title)

    html_file = open(path, 'w')
    html_file.write(html)
    html_file.close()
    print "Fixed."

def main(root_path, pages_list_filepath):
  pages_list_file = open(pages_list_filepath, 'r')
  pages_list = pages_list_file.readlines()
  pages_list_file.close()
  for i in range(len(pages_list)):
    pages_list[i] = pages_list[i].replace('\n', '')
  fix_pages(root_path, pages_list)
  
if __name__ == '__main__':
  main(sys.argv[1], sys.argv[2])
