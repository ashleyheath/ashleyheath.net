<h1>Style a terminal in CSS</h1>

<p>This is a nice effect to use if you're demonstrating the use of command-line applications or walking readers through an install process etc.</p>
<p>This is the effect we're going to achieve:</p>

<kbd  class="terminal top">echo Hello world!</kbd>
<samp class="terminal bottom">Hello world!</samp>

<p>The example above uses the following HTML and some CSS that will be explained shortly.</p>

<code programming_language="HTML" class="code left">
<ol>
  <li>&#060;kbd  class="terminal top"&#062;echo Hello world!&#060;/kbd&#062;</li>
  <li>&#060;samp class="terminal bottom">Hello world!&#060;/samp&#062;</li>
</ol>
</code>

<p>Now the explanation. First we need to create a CSS class to store the basic style attributes for the effect. We'll name it terminal.</p>

<code programming_language="CSS" class="code right">
<ol>
  <li>.terminal</li>
  <li>{</li>
  <li>  display:block;</li>
  <li>  width:94%;</li>
  <li>  margin:0em 1em 0em 1em;</li>
  <li>  padding:0em 1em 0em 1em;</li>
  <li>  background-color:black;</li>
  <li>  color:white;</li>
  <li>  line-height:150%;</li>
  <li>  font-weight:700;</li>
  <li>  border-style:solid;</li>
  <li>  border-color:rgba(255,255,0,0.3);</li>
  <li>}</li>
</ol>
</code>

<p>Most of this should be self-explanatory. The black <code>background-color</code> and white <code>colour</code> can be replaced with the colours of your choosing. The <code>width</code>, <code>margin</code>, <code>padding</code>, <code>line-height</code> and <code>font-weight</code> values are all there for purely aesthetic reasons and can be changed to suit your needs. The settings for <code>border-style</code> and <code>border-colour</code> are included here as they are part of the CSS for this site. This is because the border around the terminal makes it much easier to distinguish the black terminal on a dark background. If this is unnecessary for your site you can omit these lines as well as any references to <code>border-width</code> later on. The only important bit of code here is the <code>display</code> element, which is set to <code>block</code> because we want the terminal to sit on a row by itself in its container.</p>

<br/>
<p>Now for the clever bit.</p>

<code programming_language="CSS" class="code left">
<ol>
  <li>kbd.terminal:before</li>
  <li>{</li>
  <li>  color:#B6ACAC;</li>
  <li>  content:"ashley@laptop:~"attr(path)"$ ";</li>
  <li>}</li>
  <li>kbd.terminal.no_tilde:before</li>
  <li>{</li>
  <li>  color:#B6ACAC;</li>
  <li>  content:"ashley@laptop:"attr(path)"$ ";</li>
  <li>}</li>
</ol>
</code>

<p>On the first line we specify that any <code>&#060;kbd&#062;</code> element with a class of <code>terminal</code> should be preceded by the string that is the value of <code>content</code> (on line 4). Traditionally this follows the pattern of "username@hostname:current-working-directory$" but you can customise it as you please. In the example above you'll see that between the two strings of <code>content</code> there is something else that reads <code>attr(path)</code>. This is a snippet of CSS that allows you to use an attribute from an HTMl tag. In this case, it inserts the contents of an attribute <code>path</code> that is specified in the <code>&#060;kbd&#062;</code> tag. This allows us to customise the current working directory (CWD) of our terminal.</p>
<p>The second definition, beginning on line 6, adds a requirement for another class called <code>no_tilde</code>. The tilde character (~) refers the the current user's home directory and this new definition allows us to show the path without it should we wish to.<p>
<p>You'll notice that the colour of the text has been changed to be a bit lighter than the standard terminal font. This is to help the important text (i.e. the contents of <code>&#060;kbd&#062;</code>) better stand out. Also, by using the <code>:before</code> pseudo-element we make the content of the <code>&#060;kbd&#062;</code> element more semantically relevant by avoiding cluttering it with text that is not typed by the user.</p>

<br>
<p>Finally we need to set the terminal's borders and padding.</p>

<code programming_language="CSS" class="code right">
<ol>
  <li>.terminal.top</li>
  <li>{</li>
  <li>  margin-top:0.5em;</li>
  <li>  padding-top:0.7em;</li>
  <li>  border-radius:10px 10px 0px 0px;</li>
  <li>  border-width:2px 2px 0px 2px;</li>
  <li>}</li>
  <li>.terminal.middle</li>
  <li>{</li>
  <li>  border-radius:0px 0px 0px 0px;</li>
  <li>  border-width:0px 2px 0px 2px;</li>
  <li>}</li>
  <li>.terminal.bottom</li>
  <li>{</li>
  <li>  margin-bottom:0.7em;</li>
  <li>  padding-bottom:0.7em;</li>
  <li>  border-radius:0px 0px 10px 10px;</li>
  <li>  border-width:0px 2px 2px 2px;</li>
  <li>}</li>
  <li>.terminal.one_line</li>
  <li>{</li>
  <li>  padding-top:0.7em;</li>
  <li>  padding-bottom:0.7em;</li>
  <li>  border-radius:5px 5px 5px 5px;</li>
  <li>  border-width:2px 2px 2px 2px;</li>
  <li>}</li>
</ol>
</code>

<p>We create four classes: <code>top</code>, <code>middle</code>, <code>bottom</code> and <code>one_line</code>. For <code>top</code> we set the <code>margin</code> and <code>padding</code> on the top, and the <code>border-radius</code> and <code>border-width</code> on the top and sides. For <code>middle</code> we set the <code>border-radius</code> and <code>border-width</code> on just the sides and so on for <code>bottom</code> and <code>one_line</code>.</p>
<p>Then you give the <code>&#060;kbd&#062;</code> and <code>&#060;samp&#062;</code> tags in your HTML documents the relevant <code>top</code>, <code>middle</code> or <code>bottom</code> class (or <code>one_line</code> if there's only one line) depending on where they occur in the terminal. See the example HTML at the top of the page for more details. As a final note, you should use <code>&#060;kbd&#062;</code> for text the user types into the terminal and <code>&#060;samp&#062;</code> for the supposed output from programs.</p>

<p>Below is the complete CSS <a href="http://www.bitbucket.org/ashleyheath/ashleyheath.net/src/67820e57977d/site/css/classes.css">source</a>.</p>

<code programming_language="CSS" class="code left">
<ol>
  <li>.terminal</li>
  <li>{</li>
  <li>  display:block;</li>
  <li>  width:94%;</li>
  <li>  margin:0em 1em 0em 1em;</li>
  <li>  padding:0em 1em 0em 1em;</li>
  <li>  background-color:black;</li>
  <li>  color:white;</li>
  <li>  line-height:150%;</li>
  <li>  font-weight:700;</li>
  <li>  border-style:solid;</li>
  <li>  border-color:rgba(255,255,0,0.3);</li>
  <li>}</li>
  <li>kbd.terminal:before</li>
  <li>{</li>
  <li>  color:#B6ACAC;</li>
  <li>  content:"ashley@laptop:~"attr(path)"$ ";</li>
  <li>}</li>
  <li>kbd.terminal.no_tilde:before</li>
  <li>{</li>
  <li>  color:#B6ACAC;</li>
  <li>  content:"ashley@laptop:"attr(path)"$ ";</li>
  <li>}</li>
  <li>.terminal.top</li>
  <li>{</li>
  <li>  margin-top:0.5em;</li>
  <li>  padding-top:0.7em;</li>
  <li>  border-radius:10px 10px 0px 0px;</li>
  <li>  border-width:2px 2px 0px 2px;</li>
  <li>}</li>
  <li>.terminal.middle</li>
  <li>{</li>
  <li>  border-radius:0px 0px 0px 0px;</li>
  <li>  border-width:0px 2px 0px 2px;</li>
  <li>}</li>
  <li>.terminal.bottom</li>
  <li>{</li>
  <li>  margin-bottom:0.7em;</li>
  <li>  padding-bottom:0.7em;</li>
  <li>  border-radius:0px 0px 10px 10px;</li>
  <li>  border-width:0px 2px 2px 2px;</li>
  <li>}</li>
  <li>.terminal.one_line</li>
  <li>{</li>
  <li>  padding-top:0.7em;</li>
  <li>  padding-bottom:0.7em;</li>
  <li>  border-radius:5px 5px 5px 5px;</li>
  <li>  border-width:2px 2px 2px 2px;</li>
  <li>}</li>
</ol>
</code>
